import java.awt.event.MouseEvent;
import java.util.Date;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;

class EventConsumer extends Thread {
	private final Cluster cluster;
	private Session session;
	private final BlockingQueue<MouseEvent> queue;
	private BoundStatement insertBinder;
	private long id;
	private boolean processing = true;

	EventConsumer() throws Exception {
		this.cluster = 	Cluster.builder().addContactPoint("127.0.0.1").build();
		
		System.out.println("Connecting Event Consumer and Cassandra cluster ...");
		this.session = cluster.connect();

		System.out.println("Recovering last event ...");
		this.id = getLastEventId();
		this.queue = new LinkedBlockingQueue<MouseEvent>();
	}

	@Override
	public void run() {
		PreparedStatement inserter = session.prepare("insert into poc.mouse(id, event, x, y, when) values(?, ?, ?, ?, ?)");
		insertBinder = inserter.bind();
		MouseEvent event = null;

		while (processing) {
			try {
				event = queue.take(); 
				insert(event);
			} catch (Exception e) {
				System.out.println("Impossible to insert the mouse event " + (event != null ? "("+event.paramString()+")" : ""+" at database."));
				e.printStackTrace();
			}
		}
	}

	private void insert(MouseEvent event) throws Exception {
		insertBinder.setLong(0, id++);
		insertBinder.setString(1, event.paramString());
		insertBinder.setInt(2, event.getX());
		insertBinder.setInt(3, event.getY());
		insertBinder.setTimestamp(4, new Date(event.getWhen()));
		
		session.executeAsync(insertBinder);
	}
	
	private long getLastEventId() throws Exception {
		long result = 0;

		try {
			Row row = session.execute("select max(id) from poc.mouse").one();

			result = row.getLong(0);
		} catch (Exception e) {
			throw new Exception("It wasn't possible to recover last event id.", e);
		}

		return result;
	}
	
	BlockingQueue<MouseEvent> getQueue() {
		return queue;
	}
	
	void finish() {
		processing = false;
		session.close();
		cluster.close();
	}
}