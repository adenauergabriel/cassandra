import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

class UIButton extends JButton {
	UIButton(EventConsumer ec, JFrame frame) {
		super("Close");

		addActionListener(new UIButtonActionListener(ec, frame));
	}

	class UIButtonActionListener implements ActionListener {
		private EventConsumer ec;
		private JFrame frame;

		UIButtonActionListener(EventConsumer ec, JFrame frame) {
			this.ec = ec;
			this.frame = frame;
		}

		public void actionPerformed(ActionEvent event) {
			try {
				frame.dispose();
				ec.finish();
			} catch (Exception e) {
				JOptionPane.showMessageDialog(frame, "Impossible to close the cluster");
			}

			finally {
				System.exit(0);
			}
		}
	}
}