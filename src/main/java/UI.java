import java.awt.event.MouseAdapter;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class UI extends JFrame {
	UI(EventConsumer ec) {
		setSize(1000, 400);
		setTitle("PoC Cassandra");

		getContentPane().add(createPanel(ec));

		repaint();
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setVisible(true);
	}

	private JPanel createPanel(EventConsumer ec) {
		JPanel panel = new JPanel();
		MouseAdapter mouseListener = new UIMouseListener(ec.getQueue());
		panel.add(new UIButton(ec, this));

		panel.addMouseListener(mouseListener);
		panel.addMouseMotionListener(mouseListener);

		return panel;
	}

	public static void main(String... ignored) throws Exception {
		System.out.println("Creating Event Consumer and Cassandra cluster ...");
		EventConsumer eventConsumer = new EventConsumer();

		System.out.println("Starting event Consumer ...");
		eventConsumer.start();

		System.out.println("Opening user interface ...");
		new UI(eventConsumer);
	}
}