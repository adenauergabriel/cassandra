import java.awt.Frame;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.concurrent.BlockingQueue;

import javax.swing.JOptionPane;

class UIMouseListener extends MouseAdapter {
	private BlockingQueue<MouseEvent> queue;

	UIMouseListener(BlockingQueue<MouseEvent> queue) {
		this.queue = queue;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		enqueue(e);
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		enqueue(e);
	}

	@Override
	public void mouseExited(MouseEvent e) {
		enqueue(e);
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		enqueue(e);
	}

	private void enqueue(MouseEvent event) {
		try {
			queue.add(event);
		} catch (Exception e) {
			JOptionPane.showMessageDialog((Frame) event.getSource(),
					"Impossible to post the event (" + event.paramString() + ") at queue.");
		}
	}
}